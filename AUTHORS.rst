=======
Credits
=======

Development Lead
----------------

* Keith Yang <yang@keitheis.org>
* Jim Yeh (Chang-Ching) <lemonlatte@gmail.com>

Design Lead
-----------

* Art Pai <art@stylecoder.tw>

Contributors
------------

* Tzu-ping Chung
* Chia-Chi Chang

Translators
-----------

See: https://www.transifex.com/projects/p/pycon-apac-2014/
